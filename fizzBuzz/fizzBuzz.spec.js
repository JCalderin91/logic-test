const fizzBuzz = require("./fizzBuzz");

test("returns 1 for the number 1", () => {
  expect(fizzBuzz(1)).toEqual(1);
});

test("returns 'fizz' for the number 3", () => {
  expect(fizzBuzz(3)).toEqual("fizz");
});

test("returns 'buzz' for the number 5", () => {
  expect(fizzBuzz(5)).toEqual("buzz");
});

test("returns 'fizz buzz' for the number 15", () => {
  expect(fizzBuzz(15)).toEqual("fizz buzz");
});
