/*
3. Code a program (in javascript or typescript) that displays the numbers from 1 to 100 on
the screen, substituting the multiples of 3 for the word "fizz", the multiples of 5 for
"buzz" and the multiples of both, that is, the multiples of 3 and 5, by the word "fizz
buzz".
Plus: Implement a test to the task performed (Preferably with Jest). */

const fizzBuzz = (n) =>
  n % 3 === 0 ? (n % 5 === 0 ? "fizz buzz" : "fizz") : n % 5 === 0 ? "buzz" : n;

const fizzBuzzOneToOneHundred = () => {
  for (let i = 1; i <= 100; i++) {
    console.log(fizzBuzz(i));
  }
};

fizzBuzzOneToOneHundred();

module.exports = fizzBuzz;
