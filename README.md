# Logic test

## Test for frontend developer by Jesus Calderin

---

### Enviroment

- NodeJs v16

### Install packages

```
  npm install
```

### Functions

- Fibonacci Sequence
  ```
  node fibonacciSequence/fibonacciSequence.js
  ```
- Count Words
  ```
  node countWords/countWords.js
  ```
- Fizz Buzz
  ```
  node fizzBuzz/fizzBuzz.js
  ```

---

### run jest test

```
npm run test
```
