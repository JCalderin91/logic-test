/* Logic test
Delivery time: 4 hours after sending the test */

/* 1. In mathematics, the Fibonacci sequence or serie is the following infinite sequence of
natural numbers:
0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597 ...
where f (0) = 0, f (1) = 1 and f (n) = f (n-1) + f (n-2).
Code a program (in javascript or typescript) that solves for any number in the
fibonacci series.
Plus: Implement a test to the task performed(Preferably with Jest). */

const fibonacciSequence = (n) => {
  if (n === 0) return 0;
  if (n === 1) return 1;
  return fibonacciSequence(n - 1) + fibonacciSequence(n - 2);
};

for (let i = 0; i <= 20; i++) {
  console.log(fibonacciSequence(i));
}

module.exports = fibonacciSequence;
