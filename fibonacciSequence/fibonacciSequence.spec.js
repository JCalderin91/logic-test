const fibonacciSequence = require("./fibonacciSequence");

test("returns 0 for the number 0", () => {
  expect(fibonacciSequence(0)).toEqual(0);
});

test("returns 1 for the number 1", () => {
  expect(fibonacciSequence(1)).toEqual(1);
});

test("returns 1 for the number 2", () => {
  expect(fibonacciSequence(2)).toEqual(1);
});

test("returns 2 for the number 3", () => {
  expect(fibonacciSequence(3)).toEqual(2);
});

test("returns 3 for the number 4", () => {
  expect(fibonacciSequence(4)).toEqual(3);
});

test("returns 5 for the number 5", () => {
  expect(fibonacciSequence(5)).toEqual(5);
});

test("returns 8 for the number 6", () => {
  expect(fibonacciSequence(6)).toEqual(8);
});

test("returns 13 for the number 7", () => {
  expect(fibonacciSequence(7)).toEqual(13);
});
test("returns 1597 for the number 17", () => {
  expect(fibonacciSequence(17)).toEqual(1597);
});
