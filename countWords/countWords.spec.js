const countWords = require("./countWords");

test("return a value object", () => {
  text = "Hi how are things";
  expect(countWords(text)).toEqual({
    are: 1,
    hi: 1,
    how: 1,
    things: 1,
  });
});

test("return a value object", () => {
  text =
    "Hi how are things? How are you?Are you a developer? I am also a developer";
  expect(countWords(text)).toEqual({
    hi: 1,
    how: 2,
    are: 3,
    things: 1,
    you: 2,
    a: 2,
    developer: 2,
    i: 1,
    am: 1,
    also: 1,
  });
});
