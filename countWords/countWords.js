/* 2. Given a given input text, Code a program (in javascript or typescript) that displays the
number of repetitions of each word.
Sample text: "Hi how are things? How are you?Are you a developer? I am also a
developer"
Plus: Implement a test to the task performed(Preferably with Jest). */
const countWords = (text) => {
  let words = text
    .toLowerCase()
    .replace(/[^\w\s\d]/g, " ")
    .replace(/\s+/g, " ")
    .split(" ");

  let result = words.reduce((pre, curr) => {
    pre[curr] ? (pre[curr] += 1) : (pre[curr] = 1);
    return pre;
  }, {});
  return result;
};

console.log(
  countWords(
    "Hi how are things? How are you?Are you a developer? I am also a developer"
  )
);

module.exports = countWords;
